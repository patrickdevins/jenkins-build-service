build:
	@docker-compose -p build build
run:
	@docker-compose -p build up -d proxy data server
stop:
	@docker-compose -p build stop
clean:	stop
	@docker-compose -p build rm server proxy
clean-data: clean
	@docker-compose -p build rm -v data
garbage-collection: stop
	@docker images -qf dangling=true | xargs docker rmi
	@docker ps -a | grep '<none>' | xargs docker rm -f
