# Jenkins Build Service

## Structure
The structure of this repository lends itself to deployment on a variety
of hosts. The primary Jenkins image resides in the Dockerfile and 
associated files in `build-server/`. We are not exposing Jenkins via
the typical port, 8080. Instead, we expose port 80 through an NGINX
proxy service. This will allow us to scale the Jenkins server as needed
while retaining the same URL, leveraging Docker's container linking.
Build data, jobs, configuration and artifacts are stored on a separate
data server so that we can persist these elements from one instance of 
the build server to the next. Finally, there is a `build-agent` sample container to guide you through the first test job build as outlined 
later in this readme.

## Setup and Makefile
This build configuration is supported by using [Docker Compose](https://docs.docker.com/compose/), so all
`docker-compose` commands will work, as outlined in the yaml file. However,
a `makefile` is included to offer a concise and consistent way to build,
run, stop, clean and garbage-collect the various containers in proper order
and with the smallest learning curve of commands. The commands are as
follows:

`make build`
> Builds the proper containers and adds them to the "build" group. 

`make run`
> Runs the containers in the proper order.

`make stop`
> Stops the running containers started with the `run` command.

`make clean`
> Cleans up the proxy and build server, but leaves the data container.

`make clean-data`
> Cleans up the data container. (Delete action)

`make garbage-collection`
> Cleans up any dangling container images and any untagged processes.

The only pre-build configuration you will need to complete is to copy the
following files into the `build-server/certs/` directory:
```
ca.pem
ca-key.pem
cert.pem
key.pem
```
They should all be located in your `~/.docker/machine/certs` directory.
If this is the case, simply run `cp -R ~/.docker/machine/certs/* build-server/certs`
in the main project directory. This allows us to properly use TLS to 
communicate securely between our docker containers.

## Typical Workflow
Typically, the whole service will be started, stopped and cleaned with
the following sequence of commands:

`make build` // to update the builds.

`make run`

`make stop` (just stops the processes, can be restarted with `make run`) 

or `make clean` (removes the build server and proxy processes)

## Connecting to the Jenkins instance
Just navigate to the URL or IP address of the server that the container
is running on and the proxy should display the home page. If you are 
running Jenkins locally, from your Docker Command Line run
`docker-machine ip` to get the ip address and copy/paste that into a
browser window.

## Setting up the sample `build-agent`
Build slaves and agents are "ephemeral containers" for running jobs. 
See the sections *DOCKER EXECUTION MODEL* and 
*DOCKER EPHEMERAL SLAVE MODEL* in [this](http://engineering.riotgames.com/news/building-jenkins-inside-ephemeral-docker-container) article for more information on 
the basic topics of this sample build agent.

In setting up the sample build agent, first you'll need to set up the 
SSH private key. On the landing page of the Jenkins Server, click on 
`Credentials -> Global Credentials -> Add Credentials`. For _"Kind"_,
select _“SSH username with a private key”_. In the `username` field, type
"jenkins" and in `description` type "Jenkins user private key for 
ephemeral build agent". For the _"Private Key"_ field, select "Enter 
Directly" and copy the contents of the private key, `id_rsa` located in 
the `build-agent/files` directory, and paste them into the text-box. Then
click the "Ok" button.

The next step is to set up the Certificate directory from earlier. In
`Credentials -> Global Credentials -> Add Credentials`, for _"Kind"_, 
 select _"Docker Certificates Directory"_, for _"Description"_ use 
 "Certs for docker", and in the path field enter `/usr/local/etc/jenkins/certs`.
 Click "Ok".
 
The system is now set up to create the build slave/agent. From the build
server home page, select `Manage Jenkins -> Configure System` and scroll
down until you see *"Add new cloud"*. Click on it and select "Docker". A
form will expand that allows you to set up a Docker host. Many hosts can
be configured. Enter the following information:
```
Name : Local Dockerhost
URL : https://yourdockerhostip:TLSPORT (most likely to be 192.168.99.100:2376)
Credentials : Docker Certificates Directory
Connection Timeout : 5
Read Timeout : 15
```
Click "Test Connection" and you should see "Version = x.xx.x". If an error
appears, contact maintainer for more information.

On a successful connection test, we'll set up our build slave as a potential
node candidate. Click "Add Docker Template" below the section we just set
up and select "Docker Template". Enter the following information:
```
Docker Image : build_agent
Remote Filing System Root : /home/jenkins
Labels : testagent
Usage : Only build jobs with label restrictions matching this node
Launch Method : Docker SSH computer launcher
Credentials : jenkins
Remote FS root Mapping : /home/jenkins
```
Click "Save".

So now, our Jenkins Buld Server is set as the Docker host, and our buld
agent is set up as a usable node for running a build. Next, we will just
need to set up a test-job to run. On the Jenkins server's landing page,
click "New Item", enter "testjob" as the "Item Name" and "Freestyle Project"
as the project type. Click "Ok". Ensure _Restrict where this project can be run_
is checked, then enter *'testagent'* in the Label Expression. Scroll down
to "Add build step" and select "Execute shell". In the command box type
`echo "Hello World! This is baby’s first ephemeral build node!"` and click
"Save". Click "Build Now" and you should see the job execute. If something
is setup incorrectly, you may see it hang on "build queue waiting on a 
node to provision". Contact the maintainer for help troubleshooting.

## Errors running anything
You can find handy information in `Jenkins Landing Page -> Manage Jenkins -> System Log -> All Jenkins Logs`
if you run into any pitfalls concerning hanging jobs or bad configurations.

## Maintained by
Brad Prichard <brad@payzer.com>
Patrick Devins <patrick@payzer.com>